//go:build tjfoc
// +build tjfoc

package credentials

import (
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"testing"

	"github.com/tjfoc/gmtls/gmcredentials/echo"

	cmx509 "chainmaker.org/chainmaker-go/common/crypto/x509"

	cmtls "chainmaker.org/chainmaker-go/common/crypto/tls"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

const (
	port    = ":50051"
	address = "localhost:50051"
)

var end chan bool

type server struct{}

func (s *server) Echo(ctx context.Context, req *echo.EchoRequest) (*echo.EchoResponse, error) {
	return &echo.EchoResponse{Result: req.Req}, nil
}

const (
	cacrt     = "testdata/cacert.pem"
	servercrt = "testdata/servercert.pem"
	serverkey = "testdata/serverkey.pem"
	clientcrt = "testdata/usercert.pem"
	clientkey = "testdata/userkey.pem"
)

func serverRun() {
	cert, err := cmtls.LoadX509KeyPair(servercrt, serverkey)
	if err != nil {
		log.Fatal(err)
	}
	certPool := cmx509.NewCertPool()
	cacert, err := ioutil.ReadFile(cacrt)
	if err != nil {
		log.Fatal(err)
	}
	certPool.AppendCertsFromPEM(cacert)

	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("fail to listen: %v", err)
	}
	creds := NewTLS(&cmtls.Config{
		ClientAuth:   cmtls.RequireAndVerifyClientCert,
		Certificates: []cmtls.Certificate{cert},
		ClientCAs:    certPool,
	})
	s := grpc.NewServer(grpc.Creds(creds))
	echo.RegisterEchoServer(s, &server{})
	err = s.Serve(lis)
	if err != nil {
		log.Fatalf("Serve: %v", err)
	}
}

func clientRun() {
	cert, err := cmtls.LoadX509KeyPair(clientcrt, clientkey)
	if err != nil {
		log.Fatal(err)
	}
	certPool := cmx509.NewCertPool()
	cacert, err := ioutil.ReadFile(cacrt)
	if err != nil {
		log.Fatal(err)
	}
	certPool.AppendCertsFromPEM(cacert)
	creds := NewTLS(&cmtls.Config{
		ServerName:   "localhost",
		Certificates: []cmtls.Certificate{cert},
		RootCAs:      certPool,
	})
	conn, err := grpc.Dial(address, grpc.WithTransportCredentials(creds))
	if err != nil {
		log.Fatalf("cannot to connect: %v", err)
	}
	defer conn.Close()
	c := echo.NewEchoClient(conn)
	echoTest(c)
	end <- true
}

func echoTest(c echo.EchoClient) {
	r, err := c.Echo(context.Background(), &echo.EchoRequest{Req: "hello"})
	if err != nil {
		log.Fatalf("failed to echo: %v", err)
	}
	fmt.Printf("%s\n", r.Result)
}

func Test(t *testing.T) {
	end = make(chan bool, 64)
	go serverRun()
	go clientRun()
	<-end
}

//
//func TestClient(t *testing.T) {
//	clientRun()
//}
//
//func TestServer(t *testing.T) {
//	serverRun()
//}
