package x509

import (
	"crypto/ecdsa"
	"crypto/rsa"
	"crypto/x509"

	"chainmaker.org/chainmaker-go/common/crypto"
	cmEcdsa "chainmaker.org/chainmaker-go/common/crypto/asym/ecdsa"
	cmRsa "chainmaker.org/chainmaker-go/common/crypto/asym/rsa"
	"chainmaker.org/chainmaker-go/common/crypto/tjfoc/gmsm/sm2"
	tjSM2_1_4_1 "github.com/tjfoc/gmsm/sm2"
)

func (s *CertPool) ToTjfocCertPool() *sm2.CertPool {
	pool := sm2.NewCertPool()
	for _, cert := range s.certs {
		pool.AddCert(chainMakerCertToTjfocX509Cert(cert))
	}
	return pool
}

func chainMakerCertToTjfocX509Cert(cert *Certificate) *sm2.Certificate {
	var extKeyUsages []sm2.ExtKeyUsage
	for _, ext := range cert.ExtKeyUsage {
		extKeyUsages = append(extKeyUsages, sm2.ExtKeyUsage(ext))
	}
	var pub interface{}
	if cert.PublicKey.Type() == crypto.SM2 {
		sm2Pub := cert.PublicKey.ToStandardKey().(*tjSM2_1_4_1.PublicKey)
		pub = &ecdsa.PublicKey{
			Curve: sm2.P256Sm2(),
			X:     sm2Pub.X,
			Y:     sm2Pub.Y,
		}
	} else {
		pub = cert.PublicKey.ToStandardKey()
	}
	newCert := &sm2.Certificate{
		Raw:                         cert.Raw,
		RawTBSCertificate:           cert.RawTBSCertificate,
		RawSubjectPublicKeyInfo:     cert.RawSubjectPublicKeyInfo,
		RawSubject:                  cert.RawSubject,
		RawIssuer:                   cert.RawIssuer,
		Signature:                   cert.Signature,
		SignatureAlgorithm:          sm2.SignatureAlgorithm(cert.SignatureAlgorithm),
		PublicKeyAlgorithm:          sm2.PublicKeyAlgorithm(cert.PublicKeyAlgorithm),
		PublicKey:                   pub,
		Version:                     cert.Version,
		SerialNumber:                cert.SerialNumber,
		Issuer:                      cert.Issuer,
		Subject:                     cert.Subject,
		NotBefore:                   cert.NotBefore,
		NotAfter:                    cert.NotAfter,
		KeyUsage:                    sm2.KeyUsage(cert.KeyUsage),
		Extensions:                  cert.Extensions,
		ExtraExtensions:             cert.ExtraExtensions,
		UnhandledCriticalExtensions: cert.UnhandledCriticalExtensions,
		ExtKeyUsage:                 extKeyUsages,
		UnknownExtKeyUsage:          cert.UnknownExtKeyUsage,
		BasicConstraintsValid:       cert.BasicConstraintsValid,
		IsCA:                        cert.IsCA,
		MaxPathLen:                  cert.MaxPathLen,
		MaxPathLenZero:              cert.MaxPathLenZero,
		SubjectKeyId:                cert.SubjectKeyId,
		AuthorityKeyId:              cert.AuthorityKeyId,
		OCSPServer:                  cert.OCSPServer,
		IssuingCertificateURL:       cert.IssuingCertificateURL,
		DNSNames:                    cert.DNSNames,
		EmailAddresses:              cert.EmailAddresses,
		IPAddresses:                 cert.IPAddresses,
		PermittedDNSDomainsCritical: cert.PermittedDNSDomainsCritical,
		PermittedDNSDomains:         cert.PermittedDNSDomains,
		CRLDistributionPoints:       cert.CRLDistributionPoints,
		PolicyIdentifiers:           cert.PolicyIdentifiers,
	}
	return newCert
}

func tjfocX509CertToChainMakerCert(cert *sm2.Certificate) *Certificate {
	var extKeyUsages []x509.ExtKeyUsage
	for _, ext := range cert.ExtKeyUsage {
		extKeyUsages = append(extKeyUsages, x509.ExtKeyUsage(ext))
	}

	var pubKey crypto.PublicKey
	switch pub := cert.PublicKey.(type) {
	case *ecdsa.PublicKey:
		if pub.Curve == sm2.P256Sm2() {
			sm2pub := &tjSM2_1_4_1.PublicKey{
				Curve: tjSM2_1_4_1.P256Sm2(),
				X:     pub.X,
				Y:     pub.Y,
			}
			pubKey = &cmEcdsa.PublicKey{K: sm2pub}
		} else {
			pubKey = &cmEcdsa.PublicKey{K: pub}
		}
	case *rsa.PublicKey:
		pubKey = &cmRsa.PublicKey{K: pub}
	default:
		panic("key type not supported, this must never happens")
	}

	newCert := &Certificate{
		Raw:                         cert.Raw,
		RawTBSCertificate:           cert.RawTBSCertificate,
		RawSubjectPublicKeyInfo:     cert.RawSubjectPublicKeyInfo,
		RawSubject:                  cert.RawSubject,
		RawIssuer:                   cert.RawIssuer,
		Signature:                   cert.Signature,
		SignatureAlgorithm:          SignatureAlgorithm(cert.SignatureAlgorithm),
		PublicKeyAlgorithm:          PublicKeyAlgorithm(cert.PublicKeyAlgorithm),
		PublicKey:                   pubKey,
		Version:                     cert.Version,
		SerialNumber:                cert.SerialNumber,
		Issuer:                      cert.Issuer,
		Subject:                     cert.Subject,
		NotBefore:                   cert.NotBefore,
		NotAfter:                    cert.NotAfter,
		KeyUsage:                    x509.KeyUsage(cert.KeyUsage),
		Extensions:                  cert.Extensions,
		ExtraExtensions:             cert.ExtraExtensions,
		UnhandledCriticalExtensions: cert.UnhandledCriticalExtensions,
		ExtKeyUsage:                 extKeyUsages,
		UnknownExtKeyUsage:          cert.UnknownExtKeyUsage,
		BasicConstraintsValid:       cert.BasicConstraintsValid,
		IsCA:                        cert.IsCA,
		MaxPathLen:                  cert.MaxPathLen,
		MaxPathLenZero:              cert.MaxPathLenZero,
		SubjectKeyId:                cert.SubjectKeyId,
		AuthorityKeyId:              cert.AuthorityKeyId,
		OCSPServer:                  cert.OCSPServer,
		IssuingCertificateURL:       cert.IssuingCertificateURL,
		DNSNames:                    cert.DNSNames,
		EmailAddresses:              cert.EmailAddresses,
		IPAddresses:                 cert.IPAddresses,
		PermittedDNSDomainsCritical: cert.PermittedDNSDomainsCritical,
		PermittedDNSDomains:         cert.PermittedDNSDomains,
		CRLDistributionPoints:       cert.CRLDistributionPoints,
		PolicyIdentifiers:           cert.PolicyIdentifiers,
	}
	return newCert
}

func TjfocX509CertsToChainMakerCerts(chains [][]*sm2.Certificate) [][]*Certificate {
	var res [][]*Certificate
	for _, chain := range chains {
		var certs []*Certificate
		for _, cert := range chain {
			certs = append(certs, tjfocX509CertToChainMakerCert(cert))
		}
		res = append(res, certs)
	}
	return res
}
