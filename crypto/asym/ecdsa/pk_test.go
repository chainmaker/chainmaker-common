/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package ecdsa

import (
	"chainmaker.org/chainmaker-go/common/crypto"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestSM2EnDecrypt(t *testing.T) {
	msg := []byte("js")
	priv, err := New(crypto.SM2)
	require.Nil(t, err)

	pub := priv.PublicKey()
	ciphertext, err := pub.Encrypt(msg)
	require.Nil(t, err)

	decrypt, err := priv.Decrypt(ciphertext)
	require.Nil(t, err)

	require.Equal(t, decrypt, msg)
}