package opt

import (
	"sync/atomic"
	"time"
)

type PerfMetrics struct {
	name            string
	timeLineCurrent int
	timeLine        []int64
	timeLineCount   []int64
	count           int64
	close           bool
	lastTime        int64
	interval        int
}

func (m *PerfMetrics) Init(name string, interval int) {
	m.name = name
	m.interval = interval
	if m.interval > 100 {
		m.interval = 100
	}
	m.count = 0
	m.close = false
	m.timeLine = make([]int64, 100, 100)
	m.timeLineCount = make([]int64, 100, 100)
	m.timeLine = append(m.timeLine, time.Now().UnixNano())
	m.timeLineCount = append(m.timeLineCount, m.count)
	m.timeLineCurrent = 0

	go func() {

		startPoint := 0
		for {
			if m.close {
				break
			}
			m.timeLineCount[m.timeLineCurrent] = m.count
			m.timeLine[m.timeLineCurrent] = time.Now().UnixNano()

			startPoint = m.timeLineCurrent - m.interval
			if startPoint < 0 {
				startPoint += len(m.timeLine)
			}
			//if m.timeLine[startPoint] != 0 {
			//	m.OutputTPS(startPoint, m.timeLineCurrent)
			//}

			m.timeLineCurrent = (m.timeLineCurrent + 1) % len(m.timeLine)
			time.Sleep(time.Millisecond * 100)
		}
	}()
}

func (m *PerfMetrics) GetCount() int64 {
	return m.count
}

func (m *PerfMetrics) CountOnce() {
	m.count = atomic.AddInt64(&m.count, 1)
}

func (m *PerfMetrics) Close() {
	m.close = true
}
//func (m *PerfMetrics) OutputTPS(startPoint, endPoint int) {
//	tps, _ := decimal.NewFromFloat(float64(m.timeLineCount[endPoint] - m.timeLineCount[startPoint])).Div(
//		decimal.NewFromFloat(float64(m.timeLine[endPoint] - m.timeLine[startPoint]))).Mul(
//		decimal.NewFromFloat(float64(time.Second))).Float64()
//	fmt.Printf("%s's TPS is:%f \n", m.name, tps)
//}
